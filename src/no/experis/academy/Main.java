package no.experis.academy;

import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Main {

    public static void main(String[] args) throws IOException {

        while (true){
            printHelp();

            String url = "";

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter ex: 'character:jon snow' or 'house:stark of winterfell'");
            String in = br.readLine();

            String[] input = in.split(":");

                boolean searchWordAccepted = false;
                while (!searchWordAccepted){
                    if (input.length != 2){
                        printHelp();
                        askTryAgain(br);
                    } else {
                        switch (input[0]){
                            case "character":
                                searchWordAccepted = true;
                                String[] character = input[1].split(" ");
                                String formattedCharacterName = "";
                                formattedCharacterName = formatString(character);
                                url = "https://anapioficeandfire.com/api/characters/?name="+formattedCharacterName;
                                break;
                            case "house":
                                searchWordAccepted = true;
                                String[] house = input[1].split(" ");
                                String formattedHouse = "";
                                formattedHouse = formatString(house);
                                url = "https://anapioficeandfire.com/api/houses/?name=house+"+formattedHouse;
                                break;
                            default:
                                System.out.println("Not character or house");
                                System.out.println("Enter ex: 'character:jon snow' or 'house:stark'");
                                in = br.readLine();
                                input = in.split(":");
                                break;
                        }
                    }
                }

                StringBuffer response = MyGETRequest(url);

                if (response.equals(new StringBuffer("-1"))){
                    System.out.println(response);
                } else {
                    JSONArray jsonArray = new JSONArray(response.toString());
                    JSONObject jsonObj = new JSONObject();

                    System.out.println("Got " + jsonArray.length() + " results.");
                    if (jsonArray.length()==0){
                        askTryAgain(br);
                    } else {
                        switch (input[0]){
                            case "character":
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    jsonObj = jsonArray.getJSONObject(i);
                                    printCharacterStats(jsonObj);
                                    System.out.println("\n");
                                }
                                askTryAgain(br);
                                break;
                            case "house":
                                for (int i=0; i<jsonArray.length(); i++){
                                    jsonObj = jsonArray.getJSONObject(i);
                                    printHouseStats(jsonObj);
                                    System.out.println("\n");
                                }
                                askTryAgain(br);
                                break;
                        }
                    }
                }
        }
    }

    public static String formatString(String[] arr){
        String formatted = "";
        for(int i = 0; i<arr.length; i++){
            if (i < arr.length-1){
                formatted += arr[i] + "+";
            } else {
                formatted += arr[i];
            }
        }
        return formatted;
    }

    public static void askTryAgain(BufferedReader br) throws IOException{
        System.out.println("Do you want to continue/try again? y/n");
        String in = br.readLine();
        if (in.equals("y")){
            System.out.println("You want to continue");
        } else {
            System.exit(0);
        }
    }

    public static void printHelp(){
        System.out.println("To get information about a specific game of thrones character, \nplease give the character's full name when asked.");
    }

    public static StringBuffer MyGETRequest(String url) throws IOException {
        URL urlForGetRequest = new URL(url);
        String readLine = null;
        HttpURLConnection connection = (HttpURLConnection) urlForGetRequest.openConnection();
        connection.setRequestMethod("GET");
        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuffer response = new StringBuffer();
            while ((readLine = in .readLine()) != null) {
                response.append(readLine);
            } in .close();
            return response;
        } else {
            System.out.println("GET NOT WORKED");
            return new StringBuffer("-1");
        }
    }

    public static void printCharacterStats(JSONObject jsonObj){
        System.out.println(jsonObj.getString("name"));
        JSONArray aliasesArr = jsonObj.getJSONArray("aliases");
        System.out.println("Aliases(s): " + aliasesArr);
        System.out.println(jsonObj.getString("gender"));
        System.out.println("Culture: " + jsonObj.getString("culture"));
        System.out.println("Born: "+jsonObj.getString("born"));
        JSONArray titlesArr = jsonObj.getJSONArray("titles");
        System.out.println("Title(s):" + titlesArr);
    }

    public static void printHouseStats(JSONObject jsonObj){
        System.out.println(jsonObj.getString("name"));
        System.out.println(jsonObj.getString("words"));
        System.out.println("Region: " + jsonObj.getString("region"));
        System.out.println("Coat of arms: " +jsonObj.getString("coatOfArms"));
    }
}
